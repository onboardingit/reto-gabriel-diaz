package com.consultec.oneapi.backend.services;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.consultec.oneapi.backend.error.ClientNotFoundException;
import com.consultec.oneapi.backend.models.Quote;
import com.consultec.oneapi.backend.models.QuotesDTO;

@Service
public class QuoteService {
	
	@Autowired
	private ServiceFactory serviceFactory;
	
	private final int PAGE_LIMIT = 25;
	
	public QuotesDTO getQuotes(String sort, Long page) {
		Map<String, String> params = new HashMap();
		//params.put("limit", "25");
		//params.put("page", page != null ? page.toString() : "1");
		QuotesDTO quotes = ((Serviceable<QuotesDTO, Map<String, String>>) 
				this.serviceFactory.getService(ServicesName.QuotesRequesterService)).execute(params);
		String sortType = ServiceUtils.getSortType(sort);
		Comparator<Quote> comparator = getQuoteSortedByMovieNameComparator();
		quotes.getDocs().sort(sortType.equals("desc") ? comparator.reversed() : comparator);
		quotes.setPages((long) Math.ceil(quotes.getDocs().size() / PAGE_LIMIT));
		quotes.setPage(page);
		try {
			quotes.setDocs(this.getPage(PAGE_LIMIT, page != null ? page.intValue() : 1, quotes.getDocs()));
		} catch (IndexOutOfBoundsException e) {
			throw new ClientNotFoundException();
		}
		return quotes;
	}
	
	private List<Quote> getPage(int limit, int page, List<Quote> quotes) throws IndexOutOfBoundsException {
		int from = (page - 1) * limit;
		int to = (page * limit) - 1;
		return quotes.subList(from, to);
	}
	
	private Comparator<Quote> getQuoteSortedByMovieNameComparator() {
		Comparator<Quote> compareByMovieName = (quote1, quote2) -> {
			return quote1.getMovieName().compareTo(quote2.getMovieName());
		};
		return compareByMovieName;
	}

}
