package com.consultec.oneapi.backend.models;

import java.util.Objects;
import com.consultec.oneapi.backend.models.Character;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * CharactersDTO
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-11T10:19:04.238112400-03:00[America/Santiago]")


public class CharactersDTO   {
  @JsonProperty("docs")
  @Valid
  private List<Character> docs = null;

  @JsonProperty("page")
  private Long page = null;

  @JsonProperty("pages")
  private Long pages = null;

  public CharactersDTO docs(List<Character> docs) {
    this.docs = docs;
    return this;
  }

  public CharactersDTO addDocsItem(Character docsItem) {
    if (this.docs == null) {
      this.docs = new ArrayList<Character>();
    }
    this.docs.add(docsItem);
    return this;
  }

  /**
   * Get docs
   * @return docs
   **/
  @Schema(description = "")
      @Valid
    public List<Character> getDocs() {
    return docs;
  }

  public void setDocs(List<Character> docs) {
    this.docs = docs;
  }

  public CharactersDTO page(Long page) {
    this.page = page;
    return this;
  }

  /**
   * Get page
   * @return page
   **/
  @Schema(description = "")
  
    public Long getPage() {
    return page;
  }

  public void setPage(Long page) {
    this.page = page;
  }

  public CharactersDTO pages(Long pages) {
    this.pages = pages;
    return this;
  }

  /**
   * Get pages
   * @return pages
   **/
  @Schema(description = "")
  
    public Long getPages() {
    return pages;
  }

  public void setPages(Long pages) {
    this.pages = pages;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CharactersDTO charactersDTO = (CharactersDTO) o;
    return Objects.equals(this.docs, charactersDTO.docs) &&
        Objects.equals(this.page, charactersDTO.page) &&
        Objects.equals(this.pages, charactersDTO.pages);
  }

  @Override
  public int hashCode() {
    return Objects.hash(docs, page, pages);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CharactersDTO {\n");
    
    sb.append("    docs: ").append(toIndentedString(docs)).append("\n");
    sb.append("    page: ").append(toIndentedString(page)).append("\n");
    sb.append("    pages: ").append(toIndentedString(pages)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
