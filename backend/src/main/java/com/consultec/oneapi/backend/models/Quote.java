package com.consultec.oneapi.backend.models;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Quote
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-11T10:19:04.238112400-03:00[America/Santiago]")


public class Quote   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("dialog")
  private String dialog = null;

  @JsonProperty("movie_name")
  private String movieName = null;

  @JsonProperty("character_name")
  private String characterName = null;

  public Quote id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
   **/
  @Schema(description = "")
  
    public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Quote dialog(String dialog) {
    this.dialog = dialog;
    return this;
  }

  /**
   * Get dialog
   * @return dialog
   **/
  @Schema(description = "")
  
    public String getDialog() {
    return dialog;
  }

  public void setDialog(String dialog) {
    this.dialog = dialog;
  }

  public Quote movieName(String movieName) {
    this.movieName = movieName;
    return this;
  }

  /**
   * Get movieName
   * @return movieName
   **/
  @Schema(description = "")
  
    public String getMovieName() {
    return movieName;
  }

  public void setMovieName(String movieName) {
    this.movieName = movieName;
  }

  public Quote characterName(String characterName) {
    this.characterName = characterName;
    return this;
  }

  /**
   * Get characterName
   * @return characterName
   **/
  @Schema(description = "")
  
    public String getCharacterName() {
    return characterName;
  }

  public void setCharacterName(String characterName) {
    this.characterName = characterName;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Quote quote = (Quote) o;
    return Objects.equals(this.id, quote.id) &&
        Objects.equals(this.dialog, quote.dialog) &&
        Objects.equals(this.movieName, quote.movieName) &&
        Objects.equals(this.characterName, quote.characterName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, dialog, movieName, characterName);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Quote {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    dialog: ").append(toIndentedString(dialog)).append("\n");
    sb.append("    movieName: ").append(toIndentedString(movieName)).append("\n");
    sb.append("    characterName: ").append(toIndentedString(characterName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
