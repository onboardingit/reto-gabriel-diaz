package com.consultec.oneapi.backend.services;

public interface Serviceable<R, P> {
	
	public R execute();
	
	public default R execute(P param) {
		return this.execute();
	}
	
	public ServicesName getServiceName();

}
