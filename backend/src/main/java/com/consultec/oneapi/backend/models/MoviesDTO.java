package com.consultec.oneapi.backend.models;

import java.util.Objects;
import com.consultec.oneapi.backend.models.Movie;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * MoviesDTO
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-11T10:19:04.238112400-03:00[America/Santiago]")


public class MoviesDTO   {
  @JsonProperty("docs")
  @Valid
  private List<Movie> docs = null;

  public MoviesDTO docs(List<Movie> docs) {
    this.docs = docs;
    return this;
  }

  public MoviesDTO addDocsItem(Movie docsItem) {
    if (this.docs == null) {
      this.docs = new ArrayList<Movie>();
    }
    this.docs.add(docsItem);
    return this;
  }

  /**
   * Get docs
   * @return docs
   **/
  @Schema(description = "")
      @Valid
    public List<Movie> getDocs() {
    return docs;
  }

  public void setDocs(List<Movie> docs) {
    this.docs = docs;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MoviesDTO moviesDTO = (MoviesDTO) o;
    return Objects.equals(this.docs, moviesDTO.docs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(docs);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class MoviesDTO {\n");
    
    sb.append("    docs: ").append(toIndentedString(docs)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
