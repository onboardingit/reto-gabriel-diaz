package com.consultec.oneapi.backend.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.consultec.oneapi.backend.models.Chapter;
import com.consultec.oneapi.backend.models.CharactersDTO;
import com.consultec.oneapi.backend.models.LoginDTO;
import com.consultec.oneapi.backend.models.Movie;
import com.consultec.oneapi.backend.models.QuotesDTO;
import com.consultec.oneapi.backend.models.TokenDTO;
import com.consultec.oneapi.backend.services.ChapterService;
import com.consultec.oneapi.backend.services.CharacterService;
import com.consultec.oneapi.backend.services.LoginService;
import com.consultec.oneapi.backend.services.MovieService;
import com.consultec.oneapi.backend.services.QuoteService;

@RestController()
public class APIControllers implements ApiApi {
	
	private MovieService movieService;
	private CharacterService characterService;
	private ChapterService chapterService;
	private QuoteService quoteService;
	private LoginService loginService;
	
	@Autowired
	public APIControllers(MovieService movieService, CharacterService characterService, ChapterService chapterService, QuoteService quoteService,
			LoginService loginService) {
		this.movieService = movieService;
		this.characterService = characterService;
		this.chapterService = chapterService;
		this.quoteService = quoteService;
		this.loginService = loginService;
	}

	@Override
	public ResponseEntity<List<Chapter>> getChapters(@Valid String sort) {
		return ResponseEntity
				.status(HttpStatus.OK)
				.body(this.chapterService.getChapters(sort).getDocs());
	}

	@Override
	public ResponseEntity<CharactersDTO> getCharacters(@Valid Long page) {
		return ResponseEntity
				.status(HttpStatus.OK)
				.body(this.characterService.getCharacters(page));
	}

	@Override
	public ResponseEntity<List<Movie>> getMovies() {
		return ResponseEntity
				.status(HttpStatus.OK)
				.body(this.movieService.getMovies().getDocs());
	}

	@Override
	public ResponseEntity<QuotesDTO> getQuotes(@Valid String sort, @Valid Long page) {
		return ResponseEntity
				.status(HttpStatus.OK)
				.body(this.quoteService.getQuotes(sort, page));
	}

	@Override
	public ResponseEntity healthCheck() {
		return ResponseEntity
				.status(HttpStatus.OK)
				.body("Hello, I'm healthy");
	}

	@Override
	public ResponseEntity<TokenDTO> login(@Valid LoginDTO body) {
		return ResponseEntity
				.status(HttpStatus.ACCEPTED)
				.body(this.loginService.login(body));
	}

}
