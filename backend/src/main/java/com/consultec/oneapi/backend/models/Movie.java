package com.consultec.oneapi.backend.models;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Movie
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-11T10:19:04.238112400-03:00[America/Santiago]")


public class Movie   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("runtime_in_minutes")
  private Long runtimeInMinutes = null;

  @JsonProperty("budget_in_millions")
  private Long budgetInMillions = null;

  @JsonProperty("box_office_revenue_in_millions")
  private Long boxOfficeRevenueInMillions = null;

  @JsonProperty("academy_award_wins")
  private Long academyAwardWins = null;

  @JsonProperty("score")
  private Long score = null;

  public Movie id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
   **/
  @Schema(description = "")
  
    public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Movie name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
   **/
  @Schema(description = "")
  
    public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Movie runtimeInMinutes(Long runtimeInMinutes) {
    this.runtimeInMinutes = runtimeInMinutes;
    return this;
  }

  /**
   * Get runtimeInMinutes
   * @return runtimeInMinutes
   **/
  @Schema(description = "")
  
    public Long getRuntimeInMinutes() {
    return runtimeInMinutes;
  }

  public void setRuntimeInMinutes(Long runtimeInMinutes) {
    this.runtimeInMinutes = runtimeInMinutes;
  }

  public Movie budgetInMillions(Long budgetInMillions) {
    this.budgetInMillions = budgetInMillions;
    return this;
  }

  /**
   * Get budgetInMillions
   * @return budgetInMillions
   **/
  @Schema(description = "")
  
    public Long getBudgetInMillions() {
    return budgetInMillions;
  }

  public void setBudgetInMillions(Long budgetInMillions) {
    this.budgetInMillions = budgetInMillions;
  }

  public Movie boxOfficeRevenueInMillions(Long boxOfficeRevenueInMillions) {
    this.boxOfficeRevenueInMillions = boxOfficeRevenueInMillions;
    return this;
  }

  /**
   * Get boxOfficeRevenueInMillions
   * @return boxOfficeRevenueInMillions
   **/
  @Schema(description = "")
  
    public Long getBoxOfficeRevenueInMillions() {
    return boxOfficeRevenueInMillions;
  }

  public void setBoxOfficeRevenueInMillions(Long boxOfficeRevenueInMillions) {
    this.boxOfficeRevenueInMillions = boxOfficeRevenueInMillions;
  }

  public Movie academyAwardWins(Long academyAwardWins) {
    this.academyAwardWins = academyAwardWins;
    return this;
  }

  /**
   * Get academyAwardWins
   * @return academyAwardWins
   **/
  @Schema(description = "")
  
    public Long getAcademyAwardWins() {
    return academyAwardWins;
  }

  public void setAcademyAwardWins(Long academyAwardWins) {
    this.academyAwardWins = academyAwardWins;
  }

  public Movie score(Long score) {
    this.score = score;
    return this;
  }

  /**
   * Get score
   * @return score
   **/
  @Schema(description = "")
  
    public Long getScore() {
    return score;
  }

  public void setScore(Long score) {
    this.score = score;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Movie movie = (Movie) o;
    return Objects.equals(this.id, movie.id) &&
        Objects.equals(this.name, movie.name) &&
        Objects.equals(this.runtimeInMinutes, movie.runtimeInMinutes) &&
        Objects.equals(this.budgetInMillions, movie.budgetInMillions) &&
        Objects.equals(this.boxOfficeRevenueInMillions, movie.boxOfficeRevenueInMillions) &&
        Objects.equals(this.academyAwardWins, movie.academyAwardWins) &&
        Objects.equals(this.score, movie.score);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, runtimeInMinutes, budgetInMillions, boxOfficeRevenueInMillions, academyAwardWins, score);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Movie {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    runtimeInMinutes: ").append(toIndentedString(runtimeInMinutes)).append("\n");
    sb.append("    budgetInMillions: ").append(toIndentedString(budgetInMillions)).append("\n");
    sb.append("    boxOfficeRevenueInMillions: ").append(toIndentedString(boxOfficeRevenueInMillions)).append("\n");
    sb.append("    academyAwardWins: ").append(toIndentedString(academyAwardWins)).append("\n");
    sb.append("    score: ").append(toIndentedString(score)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
