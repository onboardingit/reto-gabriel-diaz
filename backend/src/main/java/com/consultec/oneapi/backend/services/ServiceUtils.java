package com.consultec.oneapi.backend.services;

public class ServiceUtils {
	
	public static String getSortType(String sort) {
		if (sort != null && (sort.equals("asc") || sort.equals("desc"))) {
			return sort;
		}
		return "asc";
	}

}
