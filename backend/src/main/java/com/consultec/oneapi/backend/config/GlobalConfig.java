package com.consultec.oneapi.backend.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;

@Component
public class GlobalConfig {
	
	@Value("${service.bc.movies}")
	@Getter
	private String moviesEndpoint;
	
	@Value("${service.bc.characters}")
	@Getter
	private String charactersEndpoint;
	
	@Value("${service.bc.chapters}")
	@Getter
	private String chaptersEndpoint;
	
	@Value("${service.bc.quotes}")
	@Getter
	private String quotesEndpoint;

}
