package com.consultec.oneapi.backend.services;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.consultec.oneapi.backend.config.GlobalConfig;
import com.consultec.oneapi.backend.models.ChaptersDTO;

@Component
public class ChaptersRequesterService implements Serviceable<ChaptersDTO, Map<String, String>> {
	
	private RestService<?, ChaptersDTO> restService;
	
	@Autowired
	private GlobalConfig config;
	
	public ChaptersRequesterService(RestService restService) {
		this.restService = restService;
	}

	@Override
	public ChaptersDTO execute() {
		return null;
	}
	
	@Override
	public ChaptersDTO execute(Map<String, String> parameters) {
		return this.restService.getRequest(this.config.getChaptersEndpoint(), parameters, ChaptersDTO.class);
	}

	@Override
	public ServicesName getServiceName() {
		return ServicesName.ChaptersRequesterService;
	}

}
