package com.consultec.oneapi.backend.services;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.consultec.oneapi.backend.models.CharactersDTO;

@Service
public class CharacterService {
	
	@Autowired
	private ServiceFactory serviceFactory;
	
	public CharactersDTO getCharacters() {
		return this.getCharacters(null);
	}
	
	public CharactersDTO getCharacters(Long page) {
		Map<String, String> params = new HashMap();
		params.put("sort", "name:desc");
		params.put("limit", String.valueOf(100));
		params.put("page", page != null ? page.toString() : String.valueOf(1));
		return ((Serviceable<CharactersDTO, Map<String, String>>) 
				this.serviceFactory.getService(ServicesName.CharactersRequesterService)).execute(params);
	}

}
