package com.consultec.oneapi.backend.models;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Character
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-11T10:19:04.238112400-03:00[America/Santiago]")


public class Character   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("height")
  private String height = null;

  @JsonProperty("race")
  private String race = null;

  @JsonProperty("gender")
  private String gender = null;

  @JsonProperty("birth")
  private String birth = null;

  @JsonProperty("spouse")
  private String spouse = null;

  @JsonProperty("death")
  private String death = null;

  @JsonProperty("realm")
  private String realm = null;

  @JsonProperty("hair")
  private String hair = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("wiki_url")
  private String wikiUrl = null;

  public Character id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
   **/
  @Schema(description = "")
  
    public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Character height(String height) {
    this.height = height;
    return this;
  }

  /**
   * Get height
   * @return height
   **/
  @Schema(description = "")
  
    public String getHeight() {
    return height;
  }

  public void setHeight(String height) {
    this.height = height;
  }

  public Character race(String race) {
    this.race = race;
    return this;
  }

  /**
   * Get race
   * @return race
   **/
  @Schema(description = "")
  
    public String getRace() {
    return race;
  }

  public void setRace(String race) {
    this.race = race;
  }

  public Character gender(String gender) {
    this.gender = gender;
    return this;
  }

  /**
   * Get gender
   * @return gender
   **/
  @Schema(description = "")
  
    public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public Character birth(String birth) {
    this.birth = birth;
    return this;
  }

  /**
   * Get birth
   * @return birth
   **/
  @Schema(description = "")
  
    public String getBirth() {
    return birth;
  }

  public void setBirth(String birth) {
    this.birth = birth;
  }

  public Character spouse(String spouse) {
    this.spouse = spouse;
    return this;
  }

  /**
   * Get spouse
   * @return spouse
   **/
  @Schema(description = "")
  
    public String getSpouse() {
    return spouse;
  }

  public void setSpouse(String spouse) {
    this.spouse = spouse;
  }

  public Character death(String death) {
    this.death = death;
    return this;
  }

  /**
   * Get death
   * @return death
   **/
  @Schema(description = "")
  
    public String getDeath() {
    return death;
  }

  public void setDeath(String death) {
    this.death = death;
  }

  public Character realm(String realm) {
    this.realm = realm;
    return this;
  }

  /**
   * Get realm
   * @return realm
   **/
  @Schema(description = "")
  
    public String getRealm() {
    return realm;
  }

  public void setRealm(String realm) {
    this.realm = realm;
  }

  public Character hair(String hair) {
    this.hair = hair;
    return this;
  }

  /**
   * Get hair
   * @return hair
   **/
  @Schema(description = "")
  
    public String getHair() {
    return hair;
  }

  public void setHair(String hair) {
    this.hair = hair;
  }

  public Character name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
   **/
  @Schema(description = "")
  
    public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Character wikiUrl(String wikiUrl) {
    this.wikiUrl = wikiUrl;
    return this;
  }

  /**
   * Get wikiUrl
   * @return wikiUrl
   **/
  @Schema(description = "")
  
    public String getWikiUrl() {
    return wikiUrl;
  }

  public void setWikiUrl(String wikiUrl) {
    this.wikiUrl = wikiUrl;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Character character = (Character) o;
    return Objects.equals(this.id, character.id) &&
        Objects.equals(this.height, character.height) &&
        Objects.equals(this.race, character.race) &&
        Objects.equals(this.gender, character.gender) &&
        Objects.equals(this.birth, character.birth) &&
        Objects.equals(this.spouse, character.spouse) &&
        Objects.equals(this.death, character.death) &&
        Objects.equals(this.realm, character.realm) &&
        Objects.equals(this.hair, character.hair) &&
        Objects.equals(this.name, character.name) &&
        Objects.equals(this.wikiUrl, character.wikiUrl);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, height, race, gender, birth, spouse, death, realm, hair, name, wikiUrl);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Character {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    height: ").append(toIndentedString(height)).append("\n");
    sb.append("    race: ").append(toIndentedString(race)).append("\n");
    sb.append("    gender: ").append(toIndentedString(gender)).append("\n");
    sb.append("    birth: ").append(toIndentedString(birth)).append("\n");
    sb.append("    spouse: ").append(toIndentedString(spouse)).append("\n");
    sb.append("    death: ").append(toIndentedString(death)).append("\n");
    sb.append("    realm: ").append(toIndentedString(realm)).append("\n");
    sb.append("    hair: ").append(toIndentedString(hair)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    wikiUrl: ").append(toIndentedString(wikiUrl)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
