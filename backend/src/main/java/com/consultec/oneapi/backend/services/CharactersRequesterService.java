package com.consultec.oneapi.backend.services;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.consultec.oneapi.backend.config.GlobalConfig;
import com.consultec.oneapi.backend.models.CharactersDTO;
import com.consultec.oneapi.backend.models.MoviesDTO;

@Component
public class CharactersRequesterService implements Serviceable<CharactersDTO, Map<String, String>> {
	
	private RestService<?, CharactersDTO> restService;
	
	@Autowired
	private GlobalConfig config;
	
	@Autowired
	public CharactersRequesterService(RestService restService) {
		this.restService = restService;
	}

	@Override
	public CharactersDTO execute() {
		return null;
	}
	
	@Override
	public CharactersDTO execute(Map<String, String> parameters) {
		return this.restService.getRequest(this.config.getCharactersEndpoint(), parameters, CharactersDTO.class);
	}

	@Override
	public ServicesName getServiceName() {
		return ServicesName.CharactersRequesterService;
	}

}
