package com.consultec.oneapi.backend.services;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.consultec.oneapi.backend.error.ClientNotFoundException;
import com.consultec.oneapi.backend.error.ClientServiceUnavailableException;
import com.consultec.oneapi.backend.error.ClientTooManyRequestsException;

@Service
public class RestService<I extends Object, O extends Object> {
	
	private final RestTemplate restTemplate;
	private final static String AUTHORIZATION_HEADER = "Authorization";
	
	@Autowired
	public RestService(RestTemplateBuilder builder) {
		this.restTemplate = builder.build();
	}
	
	/**
	 * This function performs a get request against an specified url and returns an instance of
	 * .class received through parameters and defined on generic type.
	 * @param url - URL to be requested.
	 * @param clazz - Class type of the object that needs to be returned.
	 * @return an instance of the object specified on generic declaration.
	 */
	public O getRequest(String url, Map<String, String> queryParams, Class<O> clazz) {
		return this.getRequest(url, queryParams, clazz, null);
	}
	
	/**
	 * This function performs a get request against an specified url with authorization data 
	 * and returns an instance of .class received through parameters and defined on generic type.
	 * @param url - URL to be requested.
	 * @param clazz - Class type of the object that needs to be returned.
	 * @param authToken - Authorization token to be included at Authorization header. 
	 * @return an instance of the object specified on generic declaration.
	 */
	public O getRequest(String url, Map<String, String> queryParams, Class<O> clazz, String authToken) {
		RequestParameters<I> parameters = new RequestParameters()
				.setHeader(AUTHORIZATION_HEADER, authToken)
				.setMethod(HttpMethod.GET)
				.setUrl(url)
				.setQueryParams(queryParams);
		return this.executeRequest(parameters, clazz);
	}
	
	/**
	 * This function performs an HTTP request based on RequestParameters object 
	 * @param parameters - RequestParameters object that contains all information to perform an HTTP request.
	 * @param clazz - Class type of the object that needs to be returned.
	 * @return an instance of the object specified on generic declaration.
	 */
	public O executeRequest(RequestParameters<I> parameters, Class<O> clazz) {
		HttpHeaders headers = new HttpHeaders();
		if (!parameters.getHeaders().isEmpty()) {
			parameters.getHeaders().entrySet().forEach(entry -> {
				headers.add(entry.getKey(), entry.getValue());
			});
		}				
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(parameters.getUrl());
		if (!parameters.getQueryParams().isEmpty()) {
			parameters.getQueryParams().entrySet().forEach(entry -> {
				uriBuilder.queryParam(entry.getKey(), entry.getValue());
			});
		}		
		uriBuilder.build();
		HttpEntity entity = new HttpEntity(parameters.getBody(), headers);
		try {
			return this.restTemplate.exchange(uriBuilder.toUriString(), parameters.getHttpMethod(), entity, clazz).getBody();
		} catch (HttpClientErrorException e) {
			switch (e.getRawStatusCode()) {
				case 500:
					throw new ClientServiceUnavailableException();
				case 429:
					throw new ClientTooManyRequestsException();
				case 404:
					throw new ClientNotFoundException();
				default:
					System.out.println(e.getRawStatusCode() + ": " + e.getStatusText());
					return null;
			}			
		}		
	}

}
