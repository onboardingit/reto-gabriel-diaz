package com.consultec.oneapi.backend.services;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.consultec.oneapi.backend.models.ChaptersDTO;

@Service
public class ChapterService {
	
	@Autowired
	private ServiceFactory serviceFactory;
	
	public ChaptersDTO getChapters(String sort) {
		Map<String, String> params = new HashMap();
		params.put("sort", "chapterName:" + ServiceUtils.getSortType(sort));
		return ((Serviceable<ChaptersDTO, Map<String, String>>) 
				this.serviceFactory.getService(ServicesName.ChaptersRequesterService)).execute(params);
	}

}
