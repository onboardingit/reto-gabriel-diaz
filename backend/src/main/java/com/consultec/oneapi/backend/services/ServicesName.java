package com.consultec.oneapi.backend.services;

public enum ServicesName {

	MoviesRequesterService,
	CharactersRequesterService,
	ChaptersRequesterService,
	QuotesRequesterService
	
}
