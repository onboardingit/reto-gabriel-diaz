package com.consultec.oneapi.backend.models;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Chapter
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-11T10:19:04.238112400-03:00[America/Santiago]")


public class Chapter   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("chapter_name")
  private String chapterName = null;

  @JsonProperty("book_name")
  private String bookName = null;

  public Chapter id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
   **/
  @Schema(description = "")
  
    public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Chapter chapterName(String chapterName) {
    this.chapterName = chapterName;
    return this;
  }

  /**
   * Get chapterName
   * @return chapterName
   **/
  @Schema(description = "")
  
    public String getChapterName() {
    return chapterName;
  }

  public void setChapterName(String chapterName) {
    this.chapterName = chapterName;
  }

  public Chapter bookName(String bookName) {
    this.bookName = bookName;
    return this;
  }

  /**
   * Get bookName
   * @return bookName
   **/
  @Schema(description = "")
  
    public String getBookName() {
    return bookName;
  }

  public void setBookName(String bookName) {
    this.bookName = bookName;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Chapter chapter = (Chapter) o;
    return Objects.equals(this.id, chapter.id) &&
        Objects.equals(this.chapterName, chapter.chapterName) &&
        Objects.equals(this.bookName, chapter.bookName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, chapterName, bookName);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Chapter {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    chapterName: ").append(toIndentedString(chapterName)).append("\n");
    sb.append("    bookName: ").append(toIndentedString(bookName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
