package com.consultec.oneapi.backend.services;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpMethod;

import lombok.Getter;

public class RequestParameters<B extends Object> {
	
	@Getter
	private Map<String, String> headers;
	
	@Getter
	private HttpMethod httpMethod;
	
	@Getter
	private String url;
	
	@Getter
	private B body;
	
	@Getter
	private Map<String, String> queryParams;
	
	public RequestParameters() {
		super();
		this.headers = this.initializeMaps();
		this.queryParams = this.initializeMaps();
	}
	
	public RequestParameters<B> setHeaders(Map<String, String> headers) {
		if (headers == null) {
			this.headers = this.initializeMaps();
		} else {
			this.headers = headers;
		}		
		return this;
	}
	
	public RequestParameters<B> setHeader(String key, String value) {
		if (key == null || value == null) {
			return this;
		}
		this.headers.put(key, value);
		return this;
	}
	
	public RequestParameters<B> setMethod(HttpMethod httpMethod) {
		this.httpMethod = httpMethod;
		return this;
	}
	
	public RequestParameters<B> setUrl(String url) {
		this.url = url;
		return this;
	}
	
	public RequestParameters<B> setBody(B body) {
		this.body = body;
		return this;
	}
	
	public RequestParameters<B> setQueryParams(Map<String, String> queryParams) {
		if (queryParams == null) {
			this.queryParams = this.initializeMaps();
		} else {
			this.queryParams = queryParams;
		}		
		return this;
	}
	
	public RequestParameters<B> setQueryParam(String key, String value) {
		if (key == null || value == null) {
			return this;
		}
		this.queryParams.put(key, value);
		return this;
	}
	
	private Map<String, String> initializeMaps() {
		return new HashMap();
	}

}
