package com.consultec.oneapi.backend.services;

import java.security.Key;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.consultec.oneapi.backend.error.RequestUnauthorizedException;
import com.consultec.oneapi.backend.models.LoginDTO;
import com.consultec.oneapi.backend.models.TokenDTO;

@Service
public class LoginService {
	
	private final LoginDTO defaultLogin;
	private static final Map<String, LoginDTO> TOKENS = new HashMap();
	
	public LoginService() {
		this.defaultLogin = new LoginDTO();
		this.defaultLogin.setUser("4lb3rt0");
		this.defaultLogin.setPassword("g4l4rg4");
	}

	public TokenDTO login(LoginDTO login) {
		if (login.getUser().equals(this.defaultLogin.getUser()) && login.getPassword().equals(this.defaultLogin.getPassword())) {
			String token = this.getToken(login);
			TOKENS.put(token, login);
			TokenDTO tokenDto = new TokenDTO();
			tokenDto.setToken(token);
			return tokenDto;
		}
		throw new RequestUnauthorizedException();
	}
	
	private String getToken(LoginDTO subject) {
		try {
		    Algorithm algorithm = Algorithm.HMAC256(subject.getUser());
		    String token = JWT.create()
		        .sign(algorithm);
		    return token;
		} catch (JWTCreationException exception){
			throw new RequestUnauthorizedException();
		}
	}
	
	public boolean isValidToken(String token) {
		LoginDTO login = TOKENS.get(token);
		return login != null;
	}

}
