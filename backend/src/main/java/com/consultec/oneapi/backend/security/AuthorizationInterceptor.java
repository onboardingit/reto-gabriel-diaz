package com.consultec.oneapi.backend.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;

import com.consultec.oneapi.backend.error.RequestUnauthorizedException;
import com.consultec.oneapi.backend.services.LoginService;

@Configuration
public class AuthorizationInterceptor implements HandlerInterceptor {
	
	LoginService loginService;
	
	@Autowired
	public AuthorizationInterceptor() {
		this.loginService = new LoginService();
	}

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object obj) throws Exception {
    	System.out.println();
		switch (request.getRequestURI()) {
			case "/api/login":
			case "/api/health-check":
				return true;
		}
    	String token = request.getHeader("Authorization");
    	RequestUnauthorizedException unauthorizedException = new RequestUnauthorizedException();
    	if (token == null) {
    		throw unauthorizedException;
    	}
    	boolean isValidToken = this.loginService.isValidToken(token);
    	if (!isValidToken) {
    		throw unauthorizedException;
    	}
    	return isValidToken;
    }

}
