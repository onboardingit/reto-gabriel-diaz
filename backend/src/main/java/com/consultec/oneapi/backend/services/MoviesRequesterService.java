package com.consultec.oneapi.backend.services;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.consultec.oneapi.backend.config.GlobalConfig;
import com.consultec.oneapi.backend.models.MoviesDTO;

@Component
public class MoviesRequesterService implements Serviceable<MoviesDTO, Map<String, String>> {
	
	private RestService<?, MoviesDTO> restService;
	
	@Autowired
	private GlobalConfig config;
	
	@Autowired
	public MoviesRequesterService(RestService restService) {
		this.restService = restService;
	}

	@Override
	public MoviesDTO execute() {
		Map<String, String> params = new HashMap();
		params.put("sort", "name:asc");
		return this.execute(params);
	}
	
	@Override
	public MoviesDTO execute(Map<String, String> parameters) {
		return this.restService.getRequest(this.config.getMoviesEndpoint(), parameters, MoviesDTO.class);
	}

	@Override
	public ServicesName getServiceName() {
		return ServicesName.MoviesRequesterService;
	}

}
