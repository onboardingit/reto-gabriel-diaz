package com.consultec.oneapi.backend.error;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ClientErrorHandler {
	
	@ExceptionHandler(value = ClientServiceUnavailableException.class)
	public ResponseEntity<ClientServiceUnavailableException> internalServerError(ClientServiceUnavailableException e) {
		return ResponseEntity
				.status(e.getCode())
				.body(e);
	}
	
	@ExceptionHandler(value = ClientNotFoundException.class)
	public ResponseEntity<ClientNotFoundException> notFoundException(ClientNotFoundException e) {
		return ResponseEntity
				.status(e.getCode())
				.body(e);
	}
		
	@ExceptionHandler(value = ClientTooManyRequestsException.class)
	public ResponseEntity<ClientTooManyRequestsException> tooManyRequestsException(ClientTooManyRequestsException e) {
		return ResponseEntity
				.status(e.getCode())
				.body(e);
	}
	
	@ExceptionHandler(value = RequestUnauthorizedException.class)
	public ResponseEntity<RequestUnauthorizedException> unauthorizedRequestException(RequestUnauthorizedException e) {
		return ResponseEntity
				.status(e.getCode())
				.body(e);
	}

}
