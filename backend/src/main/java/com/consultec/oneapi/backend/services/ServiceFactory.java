package com.consultec.oneapi.backend.services;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServiceFactory {

	private Map<ServicesName, Serviceable> services;
	
	@Autowired
	public ServiceFactory(Set<Serviceable> services) {
		this.setServices(services);
	}
	
	public Serviceable getService(ServicesName serviceName) {
		return this.services.get(serviceName);
	}
	
	private void setServices(Set<Serviceable> serviceSet) {
		this.services = new HashMap<ServicesName, Serviceable>();
		serviceSet.forEach(service -> {
			this.services.put(service.getServiceName(), service);
		});
	}
	
}
