package com.consultec.oneapi.backend.error;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@JsonIgnoreProperties({ "cause", "stackTrace", "message", "suppressed", "localizedMessage" })
public class ClientNotFoundException extends RuntimeException {
	
	@JsonProperty("msg")
	@Getter
	private final String msg = "Not Found";
	
	@JsonProperty("code")
	@Getter
	private final int code = 404;

}