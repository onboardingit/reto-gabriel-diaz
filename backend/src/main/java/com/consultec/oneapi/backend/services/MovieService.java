package com.consultec.oneapi.backend.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.consultec.oneapi.backend.models.MoviesDTO;

@Service
public class MovieService {
	
	@Autowired
	private ServiceFactory serviceFactory;
	
	public MoviesDTO getMovies() {
		return ((Serviceable<MoviesDTO, ?>) this.serviceFactory.getService(ServicesName.MoviesRequesterService)).execute();
	}

}
