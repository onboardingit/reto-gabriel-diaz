package com.consultec.oneapi.backend.services;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.consultec.oneapi.backend.config.GlobalConfig;
import com.consultec.oneapi.backend.models.ChaptersDTO;
import com.consultec.oneapi.backend.models.QuotesDTO;

@Component
public class QuotesRequesterService implements Serviceable<QuotesDTO, Map<String, String>> {
	
	private RestService<?, QuotesDTO> restService;
	
	@Autowired
	private GlobalConfig config;
	
	public QuotesRequesterService(RestService restService) {
		this.restService = restService;
	}

	@Override
	public QuotesDTO execute() {
		return null;
	}
	
	@Override
	public QuotesDTO execute(Map<String, String> parameters) {
		return this.restService.getRequest(this.config.getQuotesEndpoint(), parameters, QuotesDTO.class);
	}

	@Override
	public ServicesName getServiceName() {
		return ServicesName.QuotesRequesterService;
	}

}
