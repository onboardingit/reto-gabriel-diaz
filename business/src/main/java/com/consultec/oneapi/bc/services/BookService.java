package com.consultec.oneapi.bc.services;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.consultec.oneapi.bc.models.external.BooksOneAPIDTO;

@Service
public class BookService {

	@Autowired
	private ServiceFactory serviceFactory;
	
	public BooksOneAPIDTO getBooks(String booksIds) {
		Map<String, String> params = new HashMap();
		if (booksIds != null) {
			params.put("_id", booksIds);
		}
		return ((Serviceable<BooksOneAPIDTO, Map<String, String>>) this.serviceFactory.getService(ServicesName.BooksRequesterService)).execute(params);
	}
	
}
