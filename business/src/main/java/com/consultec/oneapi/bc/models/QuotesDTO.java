package com.consultec.oneapi.bc.models;

import java.util.Objects;
import com.consultec.oneapi.bc.models.Quote;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * QuotesDTO
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-10T08:55:45.068637-03:00[America/Santiago]")


public class QuotesDTO   {
  @JsonProperty("docs")
  @Valid
  private List<Quote> docs = null;

  @JsonProperty("total")
  private Long total = null;

  @JsonProperty("limit")
  private Long limit = null;

  @JsonProperty("page")
  private Long page = null;

  @JsonProperty("pages")
  private Long pages = null;

  public QuotesDTO docs(List<Quote> docs) {
    this.docs = docs;
    return this;
  }

  public QuotesDTO addDocsItem(Quote docsItem) {
    if (this.docs == null) {
      this.docs = new ArrayList<Quote>();
    }
    this.docs.add(docsItem);
    return this;
  }

  /**
   * Get docs
   * @return docs
   **/
  @Schema(description = "")
      @Valid
    public List<Quote> getDocs() {
    return docs;
  }

  public void setDocs(List<Quote> docs) {
    this.docs = docs;
  }

  public QuotesDTO total(Long total) {
    this.total = total;
    return this;
  }

  /**
   * Get total
   * @return total
   **/
  @Schema(description = "")
  
    public Long getTotal() {
    return total;
  }

  public void setTotal(Long total) {
    this.total = total;
  }

  public QuotesDTO limit(Long limit) {
    this.limit = limit;
    return this;
  }

  /**
   * Get limit
   * @return limit
   **/
  @Schema(description = "")
  
    public Long getLimit() {
    return limit;
  }

  public void setLimit(Long limit) {
    this.limit = limit;
  }

  public QuotesDTO page(Long page) {
    this.page = page;
    return this;
  }

  /**
   * Get page
   * @return page
   **/
  @Schema(description = "")
  
    public Long getPage() {
    return page;
  }

  public void setPage(Long page) {
    this.page = page;
  }

  public QuotesDTO pages(Long pages) {
    this.pages = pages;
    return this;
  }

  /**
   * Get pages
   * @return pages
   **/
  @Schema(description = "")
  
    public Long getPages() {
    return pages;
  }

  public void setPages(Long pages) {
    this.pages = pages;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    QuotesDTO quotesDTO = (QuotesDTO) o;
    return Objects.equals(this.docs, quotesDTO.docs) &&
        Objects.equals(this.total, quotesDTO.total) &&
        Objects.equals(this.limit, quotesDTO.limit) &&
        Objects.equals(this.page, quotesDTO.page) &&
        Objects.equals(this.pages, quotesDTO.pages);
  }

  @Override
  public int hashCode() {
    return Objects.hash(docs, total, limit, page, pages);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class QuotesDTO {\n");
    
    sb.append("    docs: ").append(toIndentedString(docs)).append("\n");
    sb.append("    total: ").append(toIndentedString(total)).append("\n");
    sb.append("    limit: ").append(toIndentedString(limit)).append("\n");
    sb.append("    page: ").append(toIndentedString(page)).append("\n");
    sb.append("    pages: ").append(toIndentedString(pages)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
