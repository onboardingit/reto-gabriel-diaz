package com.consultec.oneapi.bc.services;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.consultec.oneapi.bc.models.ChaptersDTO;
import com.consultec.oneapi.bc.models.external.BooksOneAPIDTO;
import com.consultec.oneapi.bc.models.external.ChaptersOneAPIDTO;

@Service
public class ChapterService {
	
	private BookService bookService;
	
	@Autowired
	private ServiceFactory servicefactory;
	
	@Autowired
	public ChapterService(BookService bookService) {
		this.bookService = bookService;
	}
	
	public ChaptersDTO getChapters() {
		ChaptersOneAPIDTO chaptersOneApi = ((Serviceable<ChaptersOneAPIDTO, ?>) 
				this.servicefactory.getService(ServicesName.ChaptersRequesterService)).execute();
		String chaptersIds = String.join(",", chaptersOneApi.getDocs().stream().map(chapter -> chapter.getBookId()).collect(Collectors.toSet()));
		return this.setBookNamesOnChapters(this.bookService.getBooks(chaptersIds), chaptersOneApi);
	}
	
	public ChaptersDTO getChapters(String sort) {
		Map<String, String> params = new HashMap();	
		if (sort != null) {
			params.put("sort", sort);
		}		
		ChaptersOneAPIDTO chaptersOneApi = ((Serviceable<ChaptersOneAPIDTO, Map<String, String>>) 
				this.servicefactory.getService(ServicesName.ChaptersRequesterService)).execute(params);
		String chaptersIds = String.join(",", chaptersOneApi.getDocs().stream().map(chapter -> chapter.getBookId()).collect(Collectors.toSet()));
		return this.setBookNamesOnChapters(this.bookService.getBooks(chaptersIds), chaptersOneApi);
	}
	
	private ChaptersDTO setBookNamesOnChapters(BooksOneAPIDTO books, ChaptersOneAPIDTO chaptersOneApi) {
		if (books != null) {
			books.getDocs().forEach(book -> {
				chaptersOneApi.getDocs().stream()
					.filter(chapter -> chapter.getBookId().equals(book.getId()))
					.forEach(chapter -> { 
						chapter.setName(book.getName());
					});
			});
			return chaptersOneApi.toChaptersDTO();
		}
		return null;
	}

}
