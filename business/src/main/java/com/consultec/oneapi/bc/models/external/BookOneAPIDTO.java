package com.consultec.oneapi.bc.models.external;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

public class BookOneAPIDTO {

	@JsonProperty("_id")
	@Getter
	private String id = null;

	@JsonProperty("name")
	@Getter
	private String name = null;
	
}
