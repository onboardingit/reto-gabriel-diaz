package com.consultec.oneapi.bc.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;

@Component
public class GlobalConfig {
	
	@Value("${oneapi-accesskey}")
	@Getter
	private String oneAPIAccessKey;
	
	@Value("${oneapi-endpoint-movies}")
	@Getter
	private String moviesEndpoint;
	
	@Value("${oneapi-endpoint-characters}")
	@Getter
	private String charactersEndpoint;
	
	@Value("${oneapi-endpoint-books}")
	@Getter
	private String booksEndpoint;
	
	@Value("${oneapi-endpoint-chapters}")
	@Getter
	private String chaptersEndpoint;
		
	@Value("${oneapi-endpoint-quotes}")
	@Getter
	private String quotesEndpoint;

}
