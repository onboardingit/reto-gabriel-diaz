package com.consultec.oneapi.bc.services;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.consultec.oneapi.bc.models.CharactersDTO;
import com.consultec.oneapi.bc.models.external.CharactersOneAPIDTO;

@Service
public class CharacterService {
	
	@Autowired
	private ServiceFactory serviceFactory;

	public CharactersDTO getCharacters() {
		CharactersOneAPIDTO chars = ((Serviceable<CharactersOneAPIDTO, ?>) this.serviceFactory.getService(ServicesName.CharactersRequesterService))
				.execute();
		return chars.toCharactersDTO();
	}
	
	public CharactersDTO getCharacters(String charactersIds, String sort, Long limit, Long page) {
		Map<String, String> params = new HashMap();
		if (charactersIds != null) {
			params.put("_id", charactersIds);			
		}
		if (sort != null) {
			params.put("sort", sort);
		}
		if (limit != null) {
			params.put("limit", limit.toString());
		}
		if (page != null) {
			params.put("page", page.toString());
		}
		CharactersOneAPIDTO chars = ((Serviceable<CharactersOneAPIDTO, Map<String, String>>) 
				this.serviceFactory.getService(ServicesName.CharactersRequesterService)).execute(params);
		return chars.toCharactersDTO();
	}
	
}
