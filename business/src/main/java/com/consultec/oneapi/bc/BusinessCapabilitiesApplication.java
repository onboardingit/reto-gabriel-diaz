package com.consultec.oneapi.bc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BusinessCapabilitiesApplication {

	public static void main(String[] args) {
		SpringApplication.run(BusinessCapabilitiesApplication.class, args);
	}

}
