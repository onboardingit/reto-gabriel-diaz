package com.consultec.oneapi.bc.models.external;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import com.consultec.oneapi.bc.models.Movie;
import com.consultec.oneapi.bc.models.MoviesDTO;
import com.consultec.oneapi.bc.models.Quote;
import com.consultec.oneapi.bc.models.QuotesDTO;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

public class QuotesOneAPIDTO {
	
	@JsonProperty("docs")
	@Valid
	@Getter
	private List<QuoteOneAPIDTO> docs = null;

	@JsonProperty("total")
	@Getter
	private Long total = null;

	@JsonProperty("limit")
	@Getter
	private Long limit = null;

	@JsonProperty("page")
	@Getter
	private Long page = null;

	@JsonProperty("pages")
	@Getter
	private Long pages = null;
	
	public QuotesDTO toQuotesDTO() {
		QuotesDTO quotes = new QuotesDTO();
		List<Quote> quotesList = new ArrayList();
		this.docs.forEach(quote -> {
			quotesList.add(quote.toQuote());
		});		
		quotes.setDocs(quotesList);
		quotes.setTotal(this.total);
		quotes.setLimit(this.limit);
		quotes.setPage(this.page);
		quotes.setPages(this.pages);
		return quotes;
	}

}
