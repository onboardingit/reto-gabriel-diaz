package com.consultec.oneapi.bc.error;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@JsonIgnoreProperties({ "cause", "stackTrace", "message", "suppressed", "localizedMessage" })
public class ClientServiceUnavailableException extends RuntimeException {
	
	@JsonProperty("msg")
	@Getter
	private final String msg = "Service Unavailable";
	
	@JsonProperty("code")
	@Getter
	private final int code = 503;

}
