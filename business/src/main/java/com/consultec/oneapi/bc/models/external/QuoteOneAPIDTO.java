package com.consultec.oneapi.bc.models.external;

import com.consultec.oneapi.bc.models.Movie;
import com.consultec.oneapi.bc.models.Quote;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

public class QuoteOneAPIDTO {
	
	@JsonProperty("_id")
	@Getter
	@Setter
	private String id = null;
	
	@JsonProperty("dialog")
	@Getter
	@Setter
	private String dialog = null;
	
	@JsonProperty("movie")
	@Getter
	@Setter
	private String movieId = null;
	
	@JsonProperty("character")
	@Getter
	@Setter
	private String characterId = null;
	
	@Getter
	@Setter
	private String characterName = null;
	
	@Getter
	@Setter
	private String movieName = null;
	
	public Quote toQuote() {
		  Quote quote = new Quote();
		  quote.setId(this.id);
		  quote.setDialog(this.dialog);
		  quote.setMovieName(this.movieName);
		  quote.setCharacterName(this.characterName);
		  return quote;
	  }

}
