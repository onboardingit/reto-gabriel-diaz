package com.consultec.oneapi.bc.services;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.consultec.oneapi.bc.config.GlobalConfig;
import com.consultec.oneapi.bc.error.ClientNotFoundException;
import com.consultec.oneapi.bc.models.external.ChaptersOneAPIDTO;
import com.consultec.oneapi.bc.models.external.CharactersOneAPIDTO;
import com.consultec.oneapi.bc.models.external.QuotesOneAPIDTO;

@Component
public class QuotesRequesterService implements Serviceable<QuotesOneAPIDTO, Map<String, String>> {
	
	private RestService<?, QuotesOneAPIDTO> restService;
	
	@Autowired
	private GlobalConfig config;
	
	@Autowired
	public QuotesRequesterService(RestService restService) {
		this.restService = restService;
	}

	@Override
	public QuotesOneAPIDTO execute() {
		return this.restService.getRequest(this.config.getQuotesEndpoint(), null, QuotesOneAPIDTO.class, this.config.getOneAPIAccessKey());
	}
	
	@Override
	public QuotesOneAPIDTO execute(Map<String, String> parameters) {
		QuotesOneAPIDTO quotes = this.restService.getRequest(this.config.getQuotesEndpoint(), parameters, QuotesOneAPIDTO.class, 
				this.config.getOneAPIAccessKey());
		if (quotes.getDocs().size() == 0) {
			throw new ClientNotFoundException();
		}
		return quotes;
	}
	

	@Override
	public ServicesName getServiceName() {
		return ServicesName.QuotesRequesterService;
	}

}
