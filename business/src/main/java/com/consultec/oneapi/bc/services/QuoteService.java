package com.consultec.oneapi.bc.services;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.consultec.oneapi.bc.models.ChaptersDTO;
import com.consultec.oneapi.bc.models.CharactersDTO;
import com.consultec.oneapi.bc.models.MoviesDTO;
import com.consultec.oneapi.bc.models.QuotesDTO;
import com.consultec.oneapi.bc.models.external.BooksOneAPIDTO;
import com.consultec.oneapi.bc.models.external.ChaptersOneAPIDTO;
import com.consultec.oneapi.bc.models.external.QuotesOneAPIDTO;

@Service
public class QuoteService {
	
	private MovieService movieService;
	private CharacterService characterService;
	
	@Autowired
	private ServiceFactory servicefactory;
	
	@Autowired
	public QuoteService(MovieService movieService, CharacterService characterService) {
		this.movieService = movieService;
		this.characterService = characterService;
	}
	
	public QuotesDTO getQuotes() {
		QuotesOneAPIDTO quotesOneApi = ((Serviceable<QuotesOneAPIDTO, ?>) 
				this.servicefactory.getService(ServicesName.QuotesRequesterService)).execute();
		String moviesIds = String.join(",", quotesOneApi.getDocs().stream().map(quote -> quote.getMovieId()).collect(Collectors.toSet()));
		String charactersIds = String.join(",", quotesOneApi.getDocs().stream().map(quote -> quote.getCharacterId()).collect(Collectors.toSet()));
		MoviesDTO movies = this.movieService.getMovies(moviesIds, null);
		CharactersDTO characters = this.characterService.getCharacters(charactersIds, null, null, null);
		return this.setMovieAndCharacterNamesOnQuotes(movies, characters, quotesOneApi);
	}
	
	public QuotesDTO getQuotes(String sort, Long limit, Long page) {
		Map<String, String> params = new HashMap();	
		if (sort != null) {
			params.put("sort", sort);
		}
		if (limit != null) {
			params.put("limit", limit.toString());
		}
		if (page != null) {
			params.put("page", page.toString());
		}
		QuotesOneAPIDTO quotesOneApi = ((Serviceable<QuotesOneAPIDTO, Map<String, String>>) 
				this.servicefactory.getService(ServicesName.QuotesRequesterService)).execute(params);
		String moviesIds = String.join(",", quotesOneApi.getDocs().stream().map(quote -> quote.getMovieId()).collect(Collectors.toSet()));
		String charactersIds = String.join(",", quotesOneApi.getDocs().stream().map(quote -> quote.getCharacterId()).collect(Collectors.toSet()));
		MoviesDTO movies = this.movieService.getMovies(moviesIds, null);
		CharactersDTO characters = this.characterService.getCharacters(charactersIds, null, null, null);
		return this.setMovieAndCharacterNamesOnQuotes(movies, characters, quotesOneApi);
	}
	
	private QuotesDTO setMovieAndCharacterNamesOnQuotes(MoviesDTO movies, CharactersDTO characters, QuotesOneAPIDTO quotesOneApi) {
		if (movies != null) {
			movies.getDocs().forEach(movie -> {
				quotesOneApi.getDocs().stream()
					.filter(quote -> quote.getMovieId().equals(movie.getId()))
					.forEach(quote -> { 
						quote.setMovieName(movie.getName());
					});
			});
		}
		if (characters != null) {
			characters.getDocs().forEach(character -> {
				quotesOneApi.getDocs().stream()
					.filter(quote -> quote.getCharacterId().equals(character.getId()))
					.forEach(quote -> { 
						quote.setCharacterName(character.getName());
					});
			});
		}
		return quotesOneApi.toQuotesDTO();
	}

}
