package com.consultec.oneapi.bc.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.consultec.oneapi.bc.config.GlobalConfig;
import com.consultec.oneapi.bc.models.ChaptersDTO;
import com.consultec.oneapi.bc.models.CharactersDTO;
import com.consultec.oneapi.bc.models.MoviesDTO;
import com.consultec.oneapi.bc.models.QuotesDTO;
import com.consultec.oneapi.bc.services.ChapterService;
import com.consultec.oneapi.bc.services.CharacterService;
import com.consultec.oneapi.bc.services.MovieService;
import com.consultec.oneapi.bc.services.QuoteService;

@RestController()
public class OneAPIImpl implements OneapiApi {
	
	private MovieService movieService;
	private CharacterService characterService;
	private ChapterService chapterService;
	private QuoteService quoteService;
	
	@Autowired
	private GlobalConfig config;
	
	@Autowired
	public OneAPIImpl(MovieService movieService, CharacterService characterService, ChapterService chapterService, QuoteService quoteService) {
		this.movieService = movieService;
		this.characterService = characterService;
		this.chapterService = chapterService;
		this.quoteService = quoteService;
	}

	@Override
	public ResponseEntity<ChaptersDTO> getChapters(@Valid String sort) {
		if (sort != null && sort.length() > 0) {
			return ResponseEntity
					.status(HttpStatus.OK)
					.body(this.chapterService.getChapters(sort));
		}
		return ResponseEntity
				.status(HttpStatus.OK)
				.body(this.chapterService.getChapters());
	}

	@Override
	public ResponseEntity<CharactersDTO> getCharacters(@Valid String charactersIds, @Valid String sort,
			@Valid Long limit, @Valid Long page) {
		if (charactersIds != null || sort != null || limit != null || page != null) {
			if (charactersIds != null && charactersIds.length() < 12) {
				return ResponseEntity
						.status(HttpStatus.BAD_REQUEST)
						.body(null);
			}
			return ResponseEntity
					.status(HttpStatus.OK)
					.body(this.characterService.getCharacters(charactersIds, sort, limit, page));
		}
		return ResponseEntity
				.status(HttpStatus.OK)
				.body(this.characterService.getCharacters());
	}

	@Override
	public ResponseEntity<MoviesDTO> getMovies(@Valid String moviesIds, @Valid String sort) {
		if (moviesIds != null || sort != null) {
			if (moviesIds != null && moviesIds.length() < 12) {
				return ResponseEntity
						.status(HttpStatus.BAD_REQUEST)
						.body(null);
			}
			return ResponseEntity
					.status(HttpStatus.OK)
					.body(this.movieService.getMovies(moviesIds, sort));
		}
		return ResponseEntity
				.status(HttpStatus.OK)
				.body(this.movieService.getMovies());
	}

	@Override
	public ResponseEntity<QuotesDTO> getQuotes(@Valid String sort, @Valid Long limit, @Valid Long page) {
		if (sort != null || limit != null || page != null) {
			return ResponseEntity
					.status(HttpStatus.OK)
					.body(this.quoteService.getQuotes(sort, limit, page));
		}
		return ResponseEntity
				.status(HttpStatus.OK)
				.body(this.quoteService.getQuotes());    
	}

	@Override
	public ResponseEntity healthCheck() {
		// TODO Auto-generated method stub
		return ResponseEntity
				.status(HttpStatus.OK)
				.body("Hello, I'm healthy");
	}

}
