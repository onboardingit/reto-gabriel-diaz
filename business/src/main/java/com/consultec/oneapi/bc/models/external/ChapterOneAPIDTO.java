package com.consultec.oneapi.bc.models.external;

import com.consultec.oneapi.bc.models.Chapter;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

public class ChapterOneAPIDTO {

	@JsonProperty("_id")
	@Getter
	private String id = null;

	@JsonProperty("chapterName")
	@Getter
	private String chapterName = null;

	@JsonProperty("book")
	@Getter
	private String bookId = null;
	
	@Getter
	@Setter
	private String name = null;
	
	public Chapter toChapter() {
		Chapter chapter = new Chapter();
		chapter.setId(this.id);
		chapter.setChapterName(this.chapterName);
		chapter.bookName(this.name);
		return chapter;
	}
}
