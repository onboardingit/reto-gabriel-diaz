package com.consultec.oneapi.bc.models.external;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import com.consultec.oneapi.bc.models.Character;
import com.consultec.oneapi.bc.models.CharactersDTO;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

public class CharactersOneAPIDTO {
	
	  @JsonProperty("docs")
	  @Valid
	  @Getter
	  private List<CharacterOneAPIDTO> docs = null;

	  @JsonProperty("total")
	  @Getter
	  private Long total = null;

	  @JsonProperty("limit")
	  @Getter
	  private Long limit = null;

	  @JsonProperty("page")
	  @Getter
	  private Long page = null;

	  @JsonProperty("pages")
	  @Getter
	  private Long pages = null;
	  
	  public CharactersDTO toCharactersDTO() {
		  CharactersDTO chars = new CharactersDTO();
		  List<Character> charsList = new ArrayList();
		  this.docs.forEach(character -> {
			  charsList.add(character.toCharacter());
		  });
		  chars.setDocs(charsList);
		  chars.setTotal(this.total);
		  chars.setLimit(this.limit);
		  chars.setPage(this.page);
		  chars.setPages(this.pages);
		  return chars;
	  }

}
