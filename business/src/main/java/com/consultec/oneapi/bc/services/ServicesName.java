package com.consultec.oneapi.bc.services;

public enum ServicesName {
	
	MoviesRequesterService,
	CharactersRequesterService,
	BooksRequesterService,
	ChaptersRequesterService,
	QuotesRequesterService

}
