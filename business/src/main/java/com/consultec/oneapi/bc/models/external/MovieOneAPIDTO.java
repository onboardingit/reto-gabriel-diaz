package com.consultec.oneapi.bc.models.external;

import com.consultec.oneapi.bc.models.Movie;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

public class MovieOneAPIDTO {
	
	  @JsonProperty("_id")
	  @Getter
	  private String id = null;

	  @JsonProperty("name")
	  @Getter
	  private String name = null;

	  @JsonProperty("runtimeInMinutes")
	  @Getter
	  private Long runtimeInMinutes = null;

	  @JsonProperty("budgetInMillions")
	  @Getter
	  private Long budgetInMillions = null;

	  @JsonProperty("boxOfficeRevenueInMillions")
	  @Getter
	  private Long boxOfficeRevenueInMillions = null;

	  @JsonProperty("academyAwardWins")
	  @Getter
	  private Long academyAwardWins = null;

	  @JsonProperty("rottenTomatesScore")
	  @Getter
	  private Long score = null;
	  
	  public Movie toMovie() {
		  Movie movie = new Movie();
		  movie.setId(this.id);
		  movie.setName(this.name);
		  movie.setRuntimeInMinutes(this.runtimeInMinutes);
		  movie.setBudgetInMillions(this.budgetInMillions);
		  movie.setBoxOfficeRevenueInMillions(this.boxOfficeRevenueInMillions);
		  movie.setAcademyAwardWins(this.academyAwardWins);
		  movie.setScore(this.score);
		  return movie;
	  }

}
