package com.consultec.oneapi.bc.services;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.consultec.oneapi.bc.config.GlobalConfig;
import com.consultec.oneapi.bc.error.ClientNotFoundException;
import com.consultec.oneapi.bc.models.external.MoviesOneAPIDTO;

@Component
public class MoviesRequesterService implements Serviceable<MoviesOneAPIDTO, Map<String, String>> {
	
	private RestService<?, MoviesOneAPIDTO> restService;
	
	@Autowired
	private GlobalConfig config;
	
	@Autowired
	public MoviesRequesterService(RestService restService) {
		this.restService = restService;
	}

	@Override
	public MoviesOneAPIDTO execute() {				
		return this.restService.getRequest(this.config.getMoviesEndpoint(), null, MoviesOneAPIDTO.class, this.config.getOneAPIAccessKey());
	}
	
	@Override
	public MoviesOneAPIDTO execute(Map<String, String> parameters) {
		MoviesOneAPIDTO movies = this.restService.getRequest(this.config.getMoviesEndpoint(), parameters, MoviesOneAPIDTO.class, 
				this.config.getOneAPIAccessKey());
		if (movies.getDocs().size() == 0) {
			throw new ClientNotFoundException();
		}
		return movies;
	}

	@Override
	public ServicesName getServiceName() {
		return ServicesName.MoviesRequesterService;
	}
	
}
