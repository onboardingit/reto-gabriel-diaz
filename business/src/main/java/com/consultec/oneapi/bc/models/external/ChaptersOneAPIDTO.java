package com.consultec.oneapi.bc.models.external;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import com.consultec.oneapi.bc.models.Chapter;
import com.consultec.oneapi.bc.models.ChaptersDTO;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

public class ChaptersOneAPIDTO {
	
	@JsonProperty("docs")
	@Valid
	@Getter
	private List<ChapterOneAPIDTO> docs = null;
	
	public ChaptersDTO toChaptersDTO() {
		ChaptersDTO chapters = new ChaptersDTO();
		List<Chapter> chaptersList = new ArrayList();
		this.docs.forEach(chapter -> {
			chaptersList.add(chapter.toChapter());
		});
		chapters.setDocs(chaptersList);
		return chapters;
	}

}
