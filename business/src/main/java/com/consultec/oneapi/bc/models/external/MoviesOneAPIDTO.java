package com.consultec.oneapi.bc.models.external;

import java.util.ArrayList;
import java.util.List;

import com.consultec.oneapi.bc.models.Movie;
import com.consultec.oneapi.bc.models.MoviesDTO;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

public class MoviesOneAPIDTO {
	
	@JsonProperty("docs")
	@Getter
	private List<MovieOneAPIDTO> docs = null;
	
	public MoviesDTO toMoviesDTO() {
		MoviesDTO movies = new MoviesDTO();
		List<Movie> moviesList = new ArrayList();
		this.docs.forEach(movie -> {
			moviesList.add(movie.toMovie());
		});		
		movies.setDocs(moviesList);
		return movies;
	}

}
