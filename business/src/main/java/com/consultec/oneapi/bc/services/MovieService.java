package com.consultec.oneapi.bc.services;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.consultec.oneapi.bc.models.MoviesDTO;
import com.consultec.oneapi.bc.models.external.MoviesOneAPIDTO;

@Service
public class MovieService {
	
	@Autowired
	private ServiceFactory servicefactory;
	
	/**
	 * Returns the movies founded in OneAPI legacy service
	 * @return
	 */
	public MoviesDTO getMovies() {
		MoviesOneAPIDTO moviesOneapi = ((Serviceable<MoviesOneAPIDTO, ?>) this.servicefactory.getService(ServicesName.MoviesRequesterService)).execute();
		return moviesOneapi.toMoviesDTO();
	}
	
	public MoviesDTO getMovies(String moviesIds, String sort) {
		Map<String, String> params = new HashMap();	
		if (moviesIds != null) {
			params.put("_id", moviesIds);
		}
		if (sort != null) {
			params.put("sort", sort);
		}		
		MoviesOneAPIDTO moviesOneapi = ((Serviceable<MoviesOneAPIDTO, Map<String, String>>) 
				this.servicefactory.getService(ServicesName.MoviesRequesterService)).execute(params);
		return moviesOneapi.toMoviesDTO();
	}

}
