package com.consultec.oneapi.bc.models.external;

import java.util.List;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

public class BooksOneAPIDTO {
	
	@JsonProperty("docs")
	@Valid
	@Getter
	private List<BookOneAPIDTO> docs = null;

}
