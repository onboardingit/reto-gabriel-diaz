package com.consultec.oneapi.bc.services;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.consultec.oneapi.bc.config.GlobalConfig;
import com.consultec.oneapi.bc.error.ClientNotFoundException;
import com.consultec.oneapi.bc.models.external.ChaptersOneAPIDTO;

@Component
public class ChapterRequesterService implements Serviceable<ChaptersOneAPIDTO, Map<String, String>> {
	
	private RestService<?, ChaptersOneAPIDTO> restService;
	
	@Autowired
	private GlobalConfig config;
	
	@Autowired
	public ChapterRequesterService(RestService restService) {
		this.restService = restService;
	}

	@Override
	public ChaptersOneAPIDTO execute() {
		return this.restService.getRequest(this.config.getChaptersEndpoint(), null, ChaptersOneAPIDTO.class, this.config.getOneAPIAccessKey());
	}
	
	@Override
	public ChaptersOneAPIDTO execute(Map<String, String> parameters) {
		ChaptersOneAPIDTO chapters = this.restService.getRequest(this.config.getChaptersEndpoint(), parameters, ChaptersOneAPIDTO.class, 
				this.config.getOneAPIAccessKey());
		if (chapters.getDocs().size() == 0) {
			throw new ClientNotFoundException();
		}
		return chapters;
	}

	@Override
	public ServicesName getServiceName() {
		return ServicesName.ChaptersRequesterService;
	}

}
