package com.consultec.oneapi.bc.services;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.consultec.oneapi.bc.config.GlobalConfig;
import com.consultec.oneapi.bc.error.ClientNotFoundException;
import com.consultec.oneapi.bc.models.external.CharactersOneAPIDTO;

@Component
public class CharactersRequesterService implements Serviceable<CharactersOneAPIDTO, Map<String, String>> {
	
	private RestService<?, CharactersOneAPIDTO> restService;
	
	@Autowired
	private GlobalConfig config;
	
	@Autowired
	public CharactersRequesterService(RestService restService) {
		this.restService = restService;
	}
	
	@Override
	public CharactersOneAPIDTO execute() {
		return this.restService.getRequest(this.config.getCharactersEndpoint(), null, CharactersOneAPIDTO.class, this.config.getOneAPIAccessKey());
	}
	
	@Override
	public CharactersOneAPIDTO execute(Map<String, String> parameters) {
		CharactersOneAPIDTO characters = this.restService.getRequest(this.config.getCharactersEndpoint(), parameters, CharactersOneAPIDTO.class, 
				this.config.getOneAPIAccessKey());
		if (characters.getDocs().size() == 0) {
			throw new ClientNotFoundException();
		}
		return characters;
	}

	@Override
	public ServicesName getServiceName() {
		return ServicesName.CharactersRequesterService;
	}

}
