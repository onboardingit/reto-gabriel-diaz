package com.consultec.oneapi.bc.models.external;

import com.fasterxml.jackson.annotation.JsonProperty;

import com.consultec.oneapi.bc.models.Character;

public class CharacterOneAPIDTO {
	  @JsonProperty("_id")
	  private String id = null;

	  @JsonProperty("height")
	  private String height = null;

	  @JsonProperty("race")
	  private String race = null;

	  @JsonProperty("gender")
	  private String gender = null;

	  @JsonProperty("birth")
	  private String birth = null;

	  @JsonProperty("spouse")
	  private String spouse = null;

	  @JsonProperty("death")
	  private String death = null;

	  @JsonProperty("realm")
	  private String realm = null;

	  @JsonProperty("hair")
	  private String hair = null;

	  @JsonProperty("name")
	  private String name = null;

	  @JsonProperty("wikiUrl")
	  private String wikiUrl = null;
	  
	  public Character toCharacter() {
		  Character character = new Character();
		  character.setId(this.id);
		  character.setHeight(this.height);
		  character.setRace(this.race);
		  character.setGender(this.gender);
		  character.setBirth(this.birth);
		  character.setSpouse(this.spouse);
		  character.setDeath(this.death);
		  character.setRealm(this.realm);
		  character.setHair(this.hair);
		  character.setName(this.name);
		  character.setWikiUrl(this.wikiUrl);
		  return character;
	  }
}
