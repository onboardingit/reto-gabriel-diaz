package com.consultec.oneapi.bc.services;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.consultec.oneapi.bc.config.GlobalConfig;
import com.consultec.oneapi.bc.models.external.BooksOneAPIDTO;

@Component
public class BookRequesterService implements Serviceable<BooksOneAPIDTO, Map<String, String>> {
	
	private RestService<?, BooksOneAPIDTO> restService;
	
	@Autowired
	private GlobalConfig config;
	
	@Autowired
	public BookRequesterService(RestService restService) {
		this.restService = restService;
	}

	@Override
	public BooksOneAPIDTO execute() {
		return this.restService.getRequest(this.config.getBooksEndpoint(), null, BooksOneAPIDTO.class);
	}
	
	@Override
	public BooksOneAPIDTO execute(Map<String, String> parameters) {
		return this.restService.getRequest(this.config.getBooksEndpoint(), parameters, BooksOneAPIDTO.class);
	}

	@Override
	public ServicesName getServiceName() {
		return ServicesName.BooksRequesterService;
	}
	
}
